#!/usr/bin/python3

import paho.mqtt.client as mqtt
import json


def on_connect(client, userdata, flags, rc):
    print('Connected to MQTT')


class LoRaWANMQTT:
    server = '127.0.0.1'
    client = None

    def __init__(self, server):
        self.server = server
        self.client = mqtt.Client()
        self.client.on_connect = on_connect
        self.client.connect(self.server, 1883, 60)

    def proc(self, timeout):
        self.client.loop(timeout)

    def send_msg(self, msg):
        self.client.publish('rn2483/data', json.dumps(msg))
