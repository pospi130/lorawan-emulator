#!/usr/bin/python3
import rn2483


def main():
    serial = '/dev/tnt1'
    speed = 9600

    lib = rn2483.RN243(serial, speed)
    input("Press Enter to Stop ...\n")

    lib.stop_loop()


if __name__ == '__main__':
    main()

