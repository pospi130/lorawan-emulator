#!/usr/bin/python3

import paho.mqtt.client as mqtt
import json
from signal import signal, SIGINT
from sys import exit
from datetime import datetime


def handler(signal_received, frame):
    print('SIGINT or CTRL-C detected. Exiting gracefully')
    exit(0)


def on_connect(client, userdata, flags, rc):
    print('=======================================================')
    print('                  Connected to MQTT')
    print('=======================================================')
    client.subscribe('rn2483/data')


def on_message(client, userdata, msg):
    data = json.loads(str(msg.payload, 'utf-8'))
    rec_time = datetime.now()
    rec_time = rec_time.strftime("%m-%d-%Y %H:%M:%S")
    data_ascii = str(bytes.fromhex(data['msg']), 'utf-8', 'ignore')
    print('Time: {}\nMsg: {}\nMsg ASCII: {}\nFreq: {} MHz\nDR: {}\nPort: {}\nToA: {} ms'.format(rec_time, data['msg'],
                                                                                                data_ascii,
                                                                                                data['freq'] / 1000000,
                                                                                                data['dr'],
                                                                                                data['port'],
                                                                                                data['toa']))
    print('=======================================================')


def main():
    signal(SIGINT, handler)
    client = mqtt.Client()
    client.on_connect = on_connect
    client.on_message = on_message
    client.connect('127.0.0.1', 1883, 60)
    client.loop_forever()


if __name__ == '__main__':
    main()

